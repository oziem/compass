package com.example.marcinoziem.compass

import android.hardware.Sensor
import com.example.marcinoziem.compass.location.model.LatLng
import com.example.marcinoziem.compass.sensors.SensorManagerMediator
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import java.util.*

class CompassPresenterTest {

  @Mock
  private lateinit var view: CompassContract.View
  @Mock
  private lateinit var sensorManagerMediator: SensorManagerMediator
  private lateinit var presenter: CompassPresenter
  private var random: Float = 0f
  private val gravityValues = FloatArray(3)
  private val geomagneticValues = FloatArray(3)

  @Before
  fun init() {
    MockitoAnnotations.initMocks(this)
    presenter = CompassPresenter(sensorManagerMediator, view)
    random = Random().nextInt(100).toFloat()
  }

  @Test
  fun onSensorChanged_ACCELEROMETER_without_LocationUpdate_test() {
    gravityValues[0] = random
    `when`(sensorManagerMediator.getOrientationZ(gravityValues, geomagneticValues))
      .thenReturn(0f)

    presenter.onSensorChanged(Sensor.TYPE_ACCELEROMETER, gravityValues)

    verify(sensorManagerMediator).getOrientationZ(
      gravityValues.also { it[0] = it[0] * CompassPresenter.SMOOTHING_FACTOR },
      geomagneticValues)
    verify(view).rotateNeedle(0f,0f)
    verify(view).rotatePointer(0f)
  }

  @Test
  fun onSensorChanged_MAGNETIC_with_LocationUpdate_test() {
    geomagneticValues[0] = random
    `when`(sensorManagerMediator.getOrientationZ(gravityValues, geomagneticValues))
      .thenReturn(0f)

    presenter.onLocationUpdate(LatLng(random.toDouble(), .0))
    presenter.onSensorChanged(Sensor.TYPE_MAGNETIC_FIELD, geomagneticValues)

    verify(sensorManagerMediator).getOrientationZ(gravityValues,
      geomagneticValues.also { it[0] = it[0] * CompassPresenter.SMOOTHING_FACTOR })
    verify(view).rotateNeedle(0f,0f)
    verify(view).rotatePointer(180f)
  }

}
