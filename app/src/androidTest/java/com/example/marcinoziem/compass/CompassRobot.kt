package com.example.marcinoziem.compass

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.ViewInteraction
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.view.View
import android.view.ViewGroup
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.hamcrest.TypeSafeMatcher

/**
 * Created by marcinoziem on 07/04/2018 CompassBB.
 */
fun compass(func: CompassRobot.() -> Unit) = CompassRobot().apply { func() }

class CompassRobot {

  fun clickOnLatitudeBtn(): CompassRobot = apply {
    onView(
      allOf<View>(withId(R.id.latitudeBtn), withText("Latitude"), isDisplayed()))
      .perform(click())
  }

  fun clickOnLongitudeBtn(): CompassRobot = apply {
    onView(
      allOf(withId(R.id.longitudeBtn), withText("Longitude"), isDisplayed()))
      .perform(click())
  }

  fun setLatitudeText(text: String): CompassRobot = apply {
    onLatitudeEditText().perform(replaceText(text), closeSoftKeyboard())
  }

  fun setLongitudeText(text: String): CompassRobot = apply {
    onLongitudeEditText().perform(replaceText(text), closeSoftKeyboard())
  }

  fun pressLatitudeImeAction(): CompassRobot = apply {
    onLatitudeEditText().perform(pressImeActionButton())
  }

  fun pressLongitudeImeAction(): CompassRobot = apply {
    onLongitudeEditText().perform(pressImeActionButton())
  }

  private fun onLatitudeEditText(): ViewInteraction =
    onView(allOf(withId(R.id.latitudeEditText), isDisplayed()))

  private fun onLongitudeEditText(): ViewInteraction = onView(
    allOf(withId(R.id.longitudeEditText),
//      childAtPosition(
//        childAtPosition(
//          withClassName(`is`("android.widget.RelativeLayout")),
//          4),
//        1),
      isDisplayed()))

  fun checkIfPointerIsVisible() = apply {
    onView(withId(R.id.pointer)).check(ViewAssertions.matches(isDisplayed()))
  }
  fun checkIfPointerIsInvisible() = apply {
    onView(withId(R.id.pointer)).check(ViewAssertions.matches(not(isDisplayed())))
  }
}

private fun childAtPosition(
  parentMatcher: Matcher<View>, position: Int): Matcher<View> {

  return object : TypeSafeMatcher<View>() {
    override fun describeTo(description: Description) {
      description.appendText("Child at position $position in parent ")
      parentMatcher.describeTo(description)
    }

    public override fun matchesSafely(view: View): Boolean {
      val parent = view.parent
      return (parent is ViewGroup && parentMatcher.matches(parent)
        && view == parent.getChildAt(position))
    }
  }
}
