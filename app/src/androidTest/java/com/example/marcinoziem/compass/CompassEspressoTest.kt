package com.example.marcinoziem.compass

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by marcinoziem on 08/04/2018 CompassBB.
 */
@RunWith(AndroidJUnit4::class)
class CompassEspressoTest {

  @get:Rule
  val activityTestRule = ActivityTestRule(CompassActivity::class.java)

  @Test
  fun compassEditsTest() {
    compass {
      checkIfPointerIsInvisible()

      clickOnLatitudeBtn()
      setLatitudeText("56.63")
      pressLatitudeImeAction()

      checkIfPointerIsVisible()

      clickOnLongitudeBtn()
      setLongitudeText("65.3")

      clickOnLatitudeBtn()
      pressLatitudeImeAction()
    }
  }

  @Test
  fun compassEditsVanishPointerTest() {
    compass {
      checkIfPointerIsInvisible()

      clickOnLatitudeBtn()
      setLatitudeText("56.63")
      pressLatitudeImeAction()

      checkIfPointerIsVisible()

      clickOnLongitudeBtn()
      setLongitudeText("")

      clickOnLatitudeBtn()
      setLatitudeText("")
      pressLatitudeImeAction()

      checkIfPointerIsInvisible()
    }
  }
}
