package com.example.marcinoziem.compass

import com.example.marcinoziem.compass.location.model.LatLng

/**
 * Created by marcinoziem on 03/04/2018 CompassBB.
 */
interface CompassContract {
  interface View {
    fun rotateNeedle(oldAzimuth: Float, newAzimuth: Float)
    fun rotatePointer(bearing: Float)
  }

  interface Presenter {
    fun onSensorChanged(sensorType: Int, values: FloatArray)
    fun setTargetLatitude(latitude: Double)
    fun setTargetLongitude(longitude: Double)
    fun onLocationUpdate(location: LatLng)
  }
}
