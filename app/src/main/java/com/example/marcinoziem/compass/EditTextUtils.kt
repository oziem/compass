package com.example.marcinoziem.compass

import android.text.InputFilter
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView

/**
 * Created by marcinoziem on 07/04/2018 CompassBB.
 */
object EditTextUtils {

  fun getInputFilterForRange(min: Double, max: Double) =
    InputFilter { source, _, _, dest, dStart, dEnd ->
      try {
        val input = dest.subSequence(0, dStart).toString() +
          source + dest.subSequence(dEnd, dest.length)
        if (input == "-" || input.toDouble() in min..max) return@InputFilter null
      } catch (nfe: NumberFormatException) {}
      return@InputFilter ""
    }

  fun setActionOnDone(editText: EditText, action: TextView.() -> Unit) {
    editText.setOnEditorActionListener { textView, actionId, _ ->
      if (actionId != EditorInfo.IME_ACTION_DONE) return@setOnEditorActionListener false
      textView.action()
      true
    }
  }

  fun getTextAsDouble(editText: EditText): Double = editText.text.toString()
    .let { if (it.isEmpty()) .0 else it.toDouble() }
}
