package com.example.marcinoziem.compass

import android.hardware.Sensor
import com.example.marcinoziem.compass.location.model.LatLng
import com.example.marcinoziem.compass.sensors.SensorManagerMediator


class CompassPresenter(private val sensorManagerMediator: SensorManagerMediator,
                       private val view: CompassContract.View) : CompassContract.Presenter {

  companion object {
    private const val THREE_DIMENSIONAL = 3
    const val SMOOTHING_FACTOR = 0.04f
  }

  private val gravityValues = FloatArray(THREE_DIMENSIONAL)
  private val geomagneticValues = FloatArray(THREE_DIMENSIONAL)
  private var prevAzimuth = 0f

  private var bearing: Float = 0f
  private var targetLocation = LatLng()

  override fun onLocationUpdate(location: LatLng) {
    bearing = getAngleBetween2Locations(location, targetLocation).toFloat()
  }

  private fun getAngleBetween2Locations(center: LatLng, target: LatLng): Double {
    val deltaLongitude = target.longitude - center.longitude
    val deltaLatitude = target.latitude - center.latitude
    return Math.atan2(deltaLongitude, deltaLatitude)
      .let { Math.toDegrees(it) }
  }

  override fun onSensorChanged(sensorType: Int, values: FloatArray) {
    synchronized(this) {
      when (sensorType) {
        Sensor.TYPE_ACCELEROMETER -> gravityValues.smoothlyChangeTo(values)
        Sensor.TYPE_MAGNETIC_FIELD -> geomagneticValues.smoothlyChangeTo(values)
      }
      calculateAzimuth()
    }
  }

  private fun calculateAzimuth() {
    val newAzimuth = sensorManagerMediator
      .getOrientationZ(gravityValues, geomagneticValues)
      .toDouble().let(Math::toDegrees).toFloat()
      .let { 0 - it }

    view.rotatePointer(bearing + newAzimuth)
    view.rotateNeedle(prevAzimuth, newAzimuth)
    prevAzimuth = newAzimuth
  }

  private fun FloatArray.smoothlyChangeTo(observation: FloatArray) {
    if (size != observation.size) return
    forEachIndexed { index, value ->
      this[index] = smoothByBrownsSimpleExponentialSmoothing(observation[index], value)
    }
  }

  private fun smoothByBrownsSimpleExponentialSmoothing(observation: Float,
                                                       prevResult: Float): Float {
    return SMOOTHING_FACTOR * observation + (1 - SMOOTHING_FACTOR) * prevResult
  }

  override fun setTargetLatitude(latitude: Double) {
    targetLocation.latitude = latitude
  }

  override fun setTargetLongitude(longitude: Double) {
    targetLocation.longitude = longitude
  }
}
