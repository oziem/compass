package com.example.marcinoziem.compass.sensors

import android.app.Activity
import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.hardware.SensorManager.SENSOR_DELAY_GAME
import com.example.marcinoziem.compass.BuildConfig

/**
 * Created by marcinoziem on 04/04/2018 CompassBB.
 */
class SensorManagerMediatorImp(private val activity: Activity)
  : SensorManagerMediator {

  private val sensorManager by lazy {
    activity.getSystemService(Context.SENSOR_SERVICE) as SensorManager
  }
  private var sensorEventListener: SensorEventListener? = null

  private fun initSensorEventListener(onSensorChanged: (Int, FloatArray) -> Unit) {
    sensorEventListener = object : SensorEventListener {
      override fun onSensorChanged(event: SensorEvent) {
        if (BuildConfig.ESPRESSO_TEST) return
        onSensorChanged(event.sensor.type, event.values)
      }
      override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) {}
    }
  }

  override fun requestUpdatesFor(onSensorChanged: (Int, FloatArray) -> Unit,
                                 vararg sensorsTypes: Int) {
    initSensorEventListener(onSensorChanged)
    sensorsTypes.forEach { sensorManager.registerListenerFor(it) }
  }

  private fun SensorManager.registerListenerFor(sensorType: Int) = sensorEventListener?.let {
    registerListener(it, getDefaultSensor(sensorType), SENSOR_DELAY_GAME)
  }

  override fun removeAllUpdates() {
    sensorEventListener.apply { sensorManager.unregisterListener(this) }
  }

  override fun getOrientationZ(gravityValues: FloatArray, geomagneticValues: FloatArray): Float {
    val rotationMatrix = FloatArray(9)
    val inclinationMatrix = FloatArray(9)
    val success = SensorManager.getRotationMatrix(rotationMatrix, inclinationMatrix,
      gravityValues, geomagneticValues)
    if (!success) return 0f

    val orientation = FloatArray(3)
    SensorManager.getOrientation(rotationMatrix, orientation)
    return orientation[0]
  }
}
