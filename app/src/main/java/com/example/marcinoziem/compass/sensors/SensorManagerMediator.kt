package com.example.marcinoziem.compass.sensors

/**
 * Created by marcinoziem on 06/04/2018 CompassBB.
 */
interface SensorManagerMediator {
  fun requestUpdatesFor(onSensorChanged: (Int, FloatArray) -> Unit, vararg sensorsTypes: Int)
  fun removeAllUpdates()
  fun getOrientationZ(gravityValues: FloatArray, geomagneticValues: FloatArray): Float
}
