package com.example.marcinoziem.compass.location

import com.example.marcinoziem.compass.location.model.LatLng

/**
 * Created by marcinoziem on 06/04/2018 CompassBB.
 */
interface LocationClientMediator {
  fun requestLocationPermissions(requestCode: Int)
  fun removeLocationUpdates()
  fun requestLastLocation(onSuccess: (LatLng) -> Unit)
  fun requestLocationUpdates(onUpdate: (LatLng) -> Unit, onFailure: () -> Unit)
}
