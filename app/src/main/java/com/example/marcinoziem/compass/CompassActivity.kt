package com.example.marcinoziem.compass

import android.content.Context
import android.hardware.Sensor
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.Animation
import android.view.animation.RotateAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.example.marcinoziem.compass.location.LocationClientMediator
import com.example.marcinoziem.compass.location.LocationClientMediatorImp
import com.example.marcinoziem.compass.sensors.SensorManagerMediator
import com.example.marcinoziem.compass.sensors.SensorManagerMediatorImp
import com.google.android.gms.common.GoogleApiAvailability
import kotlinx.android.synthetic.main.activity_compass.*


class CompassActivity : AppCompatActivity(), CompassContract.View {

  companion object {
    private const val ROTATE_ANIMATION_DURATION = 500L
    private const val REQUEST_LOCATION_CODE = 11
  }

  private val sensorManagerMediator: SensorManagerMediator by lazy {
    SensorManagerMediatorImp(this)
  }
  private val locationClientMediator: LocationClientMediator by lazy {
    LocationClientMediatorImp(this)
  }
  private val presenter: CompassContract.Presenter by lazy {
    CompassPresenter(sensorManagerMediator, this)
  }
  private val inputMethodManager: InputMethodManager by lazy {
    getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_compass)
    initEditTexts()
  }

  override fun onStart() {
    super.onStart()
    sensorManagerMediator.requestUpdatesFor(presenter::onSensorChanged,
      Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_MAGNETIC_FIELD)
    startLocationUpdates()
  }

  override fun onStop() {
    super.onStop()
    sensorManagerMediator.removeAllUpdates()
    locationClientMediator.removeLocationUpdates()
  }

  private fun startLocationUpdates() {
    GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
      .addOnCompleteListener { task ->
        if (task.isSuccessful) {
          locationClientMediator.requestLocationUpdates(presenter::onLocationUpdate,
            onFailure = { locationClientMediator.requestLocationPermissions(REQUEST_LOCATION_CODE) })
        } else {
          hideLatLongButtons()
        }
      }
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>,
                                          grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    if (requestCode != REQUEST_LOCATION_CODE) return
    locationClientMediator.requestLocationUpdates(presenter::onLocationUpdate,
      onFailure = { hideLatLongButtons() })
  }

  override fun rotateNeedle(oldAzimuth: Float, newAzimuth: Float) {
    needle?.startAnimation(
      RotateAnimation(oldAzimuth, newAzimuth,
        Animation.RELATIVE_TO_SELF, 0.5f,
        Animation.RELATIVE_TO_SELF, 0.5f)
        .also {
          it.duration = ROTATE_ANIMATION_DURATION
          it.fillAfter = true
        }
    )
  }

  override fun rotatePointer(bearing: Float) {
    pointer.rotation = bearing
  }

  fun onLatLongButtonsClicked(view: View) {
    checkForLatLongChanges()
    hideEditTexts()
    val editText = when (view.id) {
      R.id.latitudeBtn -> latitudeEditText
      R.id.longitudeBtn -> longitudeEditText
      else -> return
    }
    editText.visibility = View.VISIBLE
    inputMethodManager.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
  }

  private fun initEditTexts() = EditTextUtils.run {
    setActionOnDone(latitudeEditText) { onLatLongEditDone() }
    setActionOnDone(longitudeEditText) { onLatLongEditDone() }
    latitudeEditText.filters = arrayOf(getInputFilterForRange(-90.0, 90.0))
    longitudeEditText.filters = arrayOf(getInputFilterForRange(-180.0, 180.0))
  }

  private fun TextView.onLatLongEditDone() {
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
    checkForLatLongChanges()
    hideEditTexts()
  }

  private fun checkForLatLongChanges() {
    if (latitudeEditText.text.isEmpty() && longitudeEditText.text.isEmpty()) {
      pointer.visibility = View.GONE
      return
    }
    presenter.setTargetLatitude(EditTextUtils.getTextAsDouble(latitudeEditText))
    presenter.setTargetLongitude(EditTextUtils.getTextAsDouble(longitudeEditText))
    locationClientMediator.requestLastLocation(presenter::onLocationUpdate)
    pointer.visibility = View.VISIBLE
  }

  private fun hideEditTexts() {
    latitudeEditText.visibility = View.INVISIBLE
    longitudeEditText.visibility = View.INVISIBLE
  }

  private fun hideLatLongButtons() {
    latitudeBtn.visibility = View.GONE
    longitudeBtn.visibility = View.GONE
  }
}
