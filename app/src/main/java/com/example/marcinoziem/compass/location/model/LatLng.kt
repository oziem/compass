package com.example.marcinoziem.compass.location.model

/**
 * Created by marcinoziem on 07/04/2018 CompassBB.
 */
data class LatLng(var latitude: Double = .0, var longitude: Double = .0)
