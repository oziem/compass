package com.example.marcinoziem.compass.location

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Activity
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import com.example.marcinoziem.compass.location.model.LatLng
import com.google.android.gms.location.*
import java.util.concurrent.TimeUnit

/**
 * Created by marcinoziem on 06/04/2018 CompassBB.
 */
class LocationClientMediatorImp(private val activity: Activity)
  : LocationClientMediator {

  private val fusedLocationClient: FusedLocationProviderClient? =
    LocationServices.getFusedLocationProviderClient(activity)
  private val locationRequest = LocationRequest.create().apply {
    interval = TimeUnit.SECONDS.toMillis(10)
    fastestInterval = TimeUnit.SECONDS.toMillis(5)
    priority = LocationRequest.PRIORITY_HIGH_ACCURACY
  }

  private var locationCallback: LocationCallback? = null

  private fun getLocationCallback(onUpdate: (LatLng) -> Unit) =
    object : LocationCallback() {
      override fun onLocationResult(locationResult: LocationResult?) {
        locationResult?.run { onUpdate(lastLocation.toLatLng()) }
      }
    }

  override fun requestLocationUpdates(onUpdate: (LatLng) -> Unit,
                                      onFailure: () -> Unit) {
    locationCallback = getLocationCallback(onUpdate)
    if (ContextCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
      fusedLocationClient?.requestLocationUpdates(locationRequest, locationCallback, null)
    } else onFailure()
  }

  override fun requestLocationPermissions(requestCode: Int) =
    ActivityCompat.requestPermissions(activity, arrayOf(ACCESS_FINE_LOCATION), requestCode)

  override fun removeLocationUpdates() {
    locationCallback?.apply { fusedLocationClient?.removeLocationUpdates(this) }
  }

  override fun requestLastLocation(onSuccess: (LatLng) -> Unit) {
    if (ContextCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
      fusedLocationClient?.apply { lastLocation.addOnSuccessListener { onSuccess(it.toLatLng()) } }
    }
  }

  private fun Location.toLatLng() = LatLng(latitude, longitude)
}
